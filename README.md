# env-dev

# ajout de Doctrine
Par défault le conteneur Docker ne contient pas les paquets Doctrine afin de rendre le conteneur indépendant
Cependant les composant doctrine sont généralement inclus pour une utilisation avec base de données
Pour ajouter les bundles liés à Doctrine, exécuter ces commandes composer
```
> composer require symfony/orm-pack
> composer require --dev symfony/maker-bundle
```