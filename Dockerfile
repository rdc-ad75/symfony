FROM php:7.4-apache-buster as build
ARG ARG_USER=app
ARG ARG_PASSWORD=app
ARG ARG_EMAIL=app@mail.com
ARG ARG_VERSION=5.3
ARG ARG_APP_FOLDER=/app

ENV USER=$ARG_USER
ENV EMAIL=$ARG_EMAIL
ENV PASSWORD=$ARG_PASSWORD
ENV VERSION=$ARG_VERSION
ENV APP_FOLDER=$ARG_APP_FOLDER
ENV APP_SHARED_FOLDER=/usr/local/share/app
ENV DEFAULT_APACHE_FOLDER=/var/www/html

# COPY ./ssl/ /root/ssl/
COPY ./apache2/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY entrypoint.sh /root/entrypoint.sh
RUN apt-get update \
	&& apt-get install -y wget vim git acl mariadb-client postgresql libpq-dev sqlite3 libsqlite3-dev\
	&& apt-get -y autoremove
RUN  	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
	&&  php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
	&&  php composer-setup.php \
	&&  php -r "unlink('composer-setup.php');" \
	&&  mv composer.phar /usr/local/bin/composer \
	&& wget https://get.symfony.com/cli/installer -O - | bash \
	&& mv /root/.symfony/bin/symfony /usr/local/bin/symfony
	
RUN    useradd -m $USER -s /bin/bash  \
	&& runuser -l $USER -c "git config --global user.email '$EMAIL'" \
	&& runuser -l $USER -c "git config --global user.name '$USER'" \
	&& mkdir -p $APP_SHARED_FOLDER \
	&& chown -R $USER:$USER $APP_SHARED_FOLDER \
	&& mkdir -p $APP_FOLDER \
	&& chown -R $USER:$USER $APP_FOLDER \
	&& runuser -l $USER -c "symfony new --full --version=$VERSION $APP_SHARED_FOLDER" \
	&& runuser -l $USER -c "cd $APP_SHARED_FOLDER && composer remove doctrine/*" \
	&& rm -rf $DEFAULT_APACHE_FOLDER \
	&& ln -sf $APP_FOLDER $DEFAULT_APACHE_FOLDER \
	&& a2ensite 000-default \
	&& docker-php-ext-install pdo pdo_mysql pdo_pgsql pdo_sqlite

EXPOSE 80
EXPOSE 443
CMD ["bash", "/root/entrypoint.sh"]
