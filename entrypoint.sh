#!/bin/bash

if [ -z "$(ls -A $APP_FOLDER)" ]; then
	echo "Empty"
	cp -RfT $APP_SHARED_FOLDER/ $APP_FOLDER
else
	echo "Not Empty. Do nothing"
fi

echo "Modification des droits dans $APP_FOLDER"
chown -R $USER:$USER $APP_FOLDER
setfacl -R -m u:www-data:rwx $APP_FOLDER
setfacl -R -m d:u:www-data:rwx $APP_FOLDER

echo "Configuration de git $USER/$EMAIL"
git config --global user.email "$EMAIL"
git config --global user.name "$USER"

# echo "Génération des certificats SSL sur la base du fichier response.txt"
# /root/ssl/generate.sh < /root/ssl/response.txt
# if [ ! -z "$(ls -A ./server*)" ]; then
	# cp ./server* /usr/local/apache2/conf/
	# sed -i \
			# -e 's/^#\(Include .*httpd-ssl.conf\)/\1/' \
			# -e 's/^#\(LoadModule .*mod_ssl.so\)/\1/' \
			# -e 's/^#\(LoadModule .*mod_socache_shmcb.so\)/\1/' \
			# /usr/local/apache2/conf/httpd.conf
# fi
# echo "Configuration du serveur apache pour $APP_FOLDER/public"
# # activation du module rewrite
# sed -i -e 's/^#\(LoadModule .*mod_rewrite.so\)/\1/' /usr/local/apache2/conf/httpd.conf
# # modification du répertoire de l'application pour le $APP_FOLDER
# #sed -i -e 's,\(.*\)".*htdocs.*"\(.*\),\1"'"${APP_FOLDER//\//\\\/}"'\/public"\2,g' /usr/local/apache2/conf/httpd.conf

echo "Ajout du pack apache pour symfony si nécessaire"
# # ajout du pack apache
runuser -l $USER -c "cd $APP_FOLDER && composer require symfony/apache-pack"
echo "Vérification installation des composants du composer.json"
# installation des composants
# utile dans le cas d'un montage volume sur /app avec in composer.json valide
runuser -l $USER -c "cd $APP_FOLDER && composer install"
echo "Lancement du serveur ..."
apache2-foreground